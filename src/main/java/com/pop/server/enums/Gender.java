package com.pop.server.enums;

public enum Gender {

    MALE,
    FEMALE,
    OTHER
}
