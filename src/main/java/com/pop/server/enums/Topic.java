package com.pop.server.enums;

import java.util.Arrays;
import java.util.List;

public enum Topic {
    ART,
    SPORT;

    public static List<Topic> getAll(){
        return Arrays.asList(ART, SPORT);
    }
}
