package com.pop.server;


import com.pop.server.configuration.ApplicationProperties;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@Slf4j
@EnableSwagger2
public class PopApplication implements CommandLineRunner {
    private static Logger logger = LoggerFactory.getLogger(PopApplication.class);

    @Autowired
    public ApplicationProperties applicationProperties;

    @Override
    public void run(String... args) {

    }

    public static void main(String[] args) {
        SpringApplication.run(PopApplication.class, args);

    }



    protected ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("API Documentation")
                .version("1.0")
                .build();
    }

    @Bean
    protected Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .paths(PathSelectors.any())
                .build().useDefaultResponseMessages(false).apiInfo(metadata());
    }

    @Bean
    public WebMvcConfigurer getWebMvcConfigurerAdapter() {
        return new WebMvcConfigurer() {
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                // Map a url to the images folder
                registry.addResourceHandler("/images/**").addResourceLocations("file:" + applicationProperties.getImagesFolder());
            }
        };
    }

    @Bean
    protected RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
