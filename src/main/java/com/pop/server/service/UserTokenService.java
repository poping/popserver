package com.pop.server.service;

import com.pop.server.pojo.UserToken;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface UserTokenService extends ElasticsearchRepository<UserToken, String> {
    List<UserToken> findByTokenOrderByLocalDateTimeDesc(String token, Pageable pageable);
    List<UserToken> findByUserIdOrderByLocalDateTimeDesc(String token, Pageable pageable);
}
