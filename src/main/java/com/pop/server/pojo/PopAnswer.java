package com.pop.server.pojo;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
@Builder
public class PopAnswer {

    public enum AnswerTypeEnum {
        OPTION1("option1"),
        OPTION2("option2"),
        DONT_KNOW("dont_know");

        private String field;

        AnswerTypeEnum(String field) {
            this.field = field;
        }

    }

    @Id
    private String id;
}
