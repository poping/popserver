package com.pop.server.pojo;

import com.pop.server.enums.Topic;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@ApiModel
public class PopQuestionRequest {
    private String description;
    private List<Topic> topics;
}