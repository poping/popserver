package com.pop.server.pojo;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

@Data
@Builder
@Document(indexName = "user_token", type = "user_token", shards = 1, replicas = 0, refreshInterval = "10s")
public class UserToken {
    @Id
    private String id;
    private String userId;
    private String token;
    @Field(type = FieldType.Date)
    private Date localDateTime;
    private PopUserTokenStatus popUserTokenStatus;
}
