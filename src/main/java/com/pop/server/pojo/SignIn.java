package com.pop.server.pojo;

import com.pop.server.enums.AccountType;
import lombok.Data;

@Data
public class SignIn {
    private String accessToken;
    private AccountType accountType;
}
