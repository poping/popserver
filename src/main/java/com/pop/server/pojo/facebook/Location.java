package com.pop.server.pojo.facebook;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Location {

    String city;

    int city_id;

    String country;

    String country_code;

    float latitude;

    int located_in;

    float longitude;

    String name;

    String region;

    int region_id;

    String state;

    String street;

    String zip;

}
