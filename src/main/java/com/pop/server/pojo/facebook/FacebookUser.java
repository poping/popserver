package com.pop.server.pojo.facebook;

import lombok.Builder;
import lombok.Data;

/**
 * This pojo represents facebook user. It includes all details returned from Facebook API
 */
@Data
@Builder
public class FacebookUser {

    /**
     * The id of this person's user account
     */
    String id;

    /**
     * The person's address and other location information
     */
    Location location;

    /**
     * The person's birthday.
     * This is a fixed format string, like MM/DD/YYYY. However, people can control who can see the year they were born
     * separately from the month and day so this string can be only the year (YYYY) or the month + day (MM/DD)
     */
    String birthday;

    /**
     * The person's first name
     */
    String first_name;

    /**
     * The gender selected by this person, male or female.
     * If the gender is set to a custom value, this value will be based off of the preferred pronoun; it will be omitted
     * if the preferred preferred pronoun is neutral
     */
    String gender;

    /**
     * The person's last name
     */
    String last_name;

    /**
     * The person's middle name
     */
    String middle_name;

    /**
     * The person's full name
     */
    String name;

    /**
     * The person's relationship status
     */
    String relationship_status;

    /**
     * The person's primary email address listed on their profile.
     * This field will not be returned if no valid email address is available
     */
    String email;

}
