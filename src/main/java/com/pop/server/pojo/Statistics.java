package com.pop.server.pojo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Statistics {

    private String description;
    private String questionId;
    private long timeToEnd;
    private long questionCreationTime;

    private Long option1;
    private Long option2;
    private Long dontKnow;

}
