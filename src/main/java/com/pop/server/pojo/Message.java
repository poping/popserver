package com.pop.server.pojo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Message {
    private String message;
}
