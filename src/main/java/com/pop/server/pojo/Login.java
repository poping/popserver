package com.pop.server.pojo;

import com.pop.server.enums.AccountType;
import lombok.Data;

@Data
public class Login {
    private String accessToken;
    private AccountType accountType;
}
