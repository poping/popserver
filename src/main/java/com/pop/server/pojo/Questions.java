package com.pop.server.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Questions {
    private List<PopQuestion> questions;
}
