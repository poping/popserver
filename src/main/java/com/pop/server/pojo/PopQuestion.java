package com.pop.server.pojo;

import com.pop.server.enums.Topic;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;

@Data
@Builder
@Document(indexName = "pop_question", type = "pop_question", shards = 1, replicas = 0, refreshInterval = "10s")
public class PopQuestion {

    @Id
    private String id;
    private String userId;
    private String description;
    private long timeToEnd;
    private long creationTime;
    private List<Topic> topics;

}