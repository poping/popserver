package com.pop.server.pojo;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Data
@Builder
@Document(indexName = "pop_answer", type = "pop_answer", shards = 1, replicas = 0, refreshInterval = "10s")
public class Answer {

    @Id
    private String id;
    private String questionId;
    private String askingUserId;
    private String answerUserId;
    private String description;
    private PopAnswer.AnswerTypeEnum answerTypeEnum;

}