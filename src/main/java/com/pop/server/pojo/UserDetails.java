package com.pop.server.pojo;

import com.pop.server.enums.AccountType;
import com.pop.server.enums.Gender;
import com.pop.server.enums.Topic;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.validation.constraints.Email;
import java.util.List;

/**
 * This Pojo represents all details of a POP user retrieved from Facebook/Google/Other login API
 */
@Data
@Builder
@Document(indexName = "user_details", type = "user_details", shards = 1, replicas = 0, refreshInterval = "10s")
public class UserDetails {

    @Id
    private String id;

    /**
     * The person's birthday.
     * This is a fixed format string, like MM/DD/YYYY. However, people can control who can see the year they were born
     * separately from the month and day so this string can be only the year (YYYY) or the month + day (MM/DD)
     */
    String birthday;

    /**
     * The person's first name
     */
    String firstName;

    /**
     * The gender selected by this person, male/female/Other
     */
    Gender gender;

    /**
     * The person's last name
     */
    String lastName;

    /**
     * The person's middle name
     */
    String middleName;

    /**
     * The person's full name
     */
    String fullName;

    /**
     * The person's primary email address listed on their profile.
     * This field will not be returned if no valid email address is available
     */
    @Email
    String email;


    /**
     * The account type of user
     */
    AccountType accountType;

    /**
     * Account id of facebook\google
     */
    String accountId;


    List<Topic> topics;
}