package com.pop.server.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pop.server.configuration.ApplicationProperties;
import com.pop.server.enums.Topic;
import com.pop.server.interfaces.PopAnswerService;
import com.pop.server.interfaces.PopQuestionService;
import com.pop.server.interfaces.PopUserService;
import com.pop.server.pojo.*;
import com.pop.server.service.UserTokenService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.pop.server.constants.ApiConstants.*;
import static com.pop.server.pojo.PopAnswer.AnswerTypeEnum.*;
import static java.awt.Image.SCALE_SMOOTH;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;
import static java.io.File.separator;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@RestController
@RequestMapping(value = "/api", produces = APPLICATION_JSON_VALUE)
@Api
@Slf4j
public class PopResource {

    private static final Logger logger = LoggerFactory.getLogger(PopResource.class);
    public static final String QUESTION = "question";

    @Autowired
    List<ElasticsearchRepository> elasticsearchRepository;

    @Autowired
    private PopQuestionService popQuestionService;

    @Autowired
    private PopAnswerService popAnswerService;

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private PopUserService popUserService;

    @Autowired
    private UserTokenService userTokenService;

    @Autowired
    private ObjectMapper mapper;

    @ApiOperation(value = "Ask a question",
            consumes = MULTIPART_FORM_DATA_VALUE,
            tags = {SWAGGER_TAG_POP})
    @PostMapping(value = "/ask")
    public PopQuestion askQuestion(
            @RequestHeader("access-token") String accessToken,
            @RequestPart(name = QUESTION) String question,
            @RequestPart(MULTIPART_FILE) MultipartFile file) throws IOException {

        // Get the user id according to the access token which must be valid since it was validated in security Config
        String userId = userTokenService.findByTokenOrderByLocalDateTimeDesc(accessToken, PageRequest.of(0, 1)).get(0).getUserId();
        long now = Instant.now().getEpochSecond();
        PopQuestionRequest popQuestionRequest = mapper.readValue(question, PopQuestionRequest.class);
        PopQuestion popQuestion = popQuestionService.save(PopQuestion.builder().userId(userId)
                .timeToEnd(now + TimeUnit.HOURS.toMillis(48))
                .description(popQuestionRequest.getDescription())
                .creationTime(Instant.now().getEpochSecond())
                .topics(popQuestionRequest.getTopics())
                .build());
        String id = popQuestion.getId();

        try (InputStream imageInputStream = file.getInputStream()) {
            BufferedImage bufferedImage = ImageIO.read(imageInputStream);

            // Image type not supported
            if (bufferedImage == null) {
                logger.warn("unsupported image file received");
            }

            int actualWidth = bufferedImage.getWidth();
            int actualHeight = bufferedImage.getHeight();

            boolean shouldScaleImage = false;

            if (actualWidth > IMAGE_MAX_WIDTH) {
                actualWidth = IMAGE_MAX_WIDTH;
                shouldScaleImage = true;
            }
            if (actualHeight > IMAGE_MAX_HEIGHT) {
                actualHeight = IMAGE_MAX_HEIGHT;
                shouldScaleImage = true;
            }

            BufferedImage imageToSave = bufferedImage;

            // Resize image: http://forum.spring.io/forum/spring-projects/web/37507-resizing-a-uploaded-image-file
            if (shouldScaleImage) {
                Image image = bufferedImage.getScaledInstance(actualWidth, actualHeight, SCALE_SMOOTH);
                int w = image.getWidth(null);
                int h = image.getHeight(null);
                BufferedImage dest = new BufferedImage(w, h, TYPE_INT_RGB);
                Graphics2D g2 = dest.createGraphics();
                g2.drawImage(image, 0, 0, null);
                g2.dispose();
                imageToSave = dest;
            }
            ImageIO.write(imageToSave, "jpg", new File(applicationProperties.getImagesFolder() + separator + id));
        }
        return popQuestion;
    }

    @ApiOperation(value = "Answer question",
            consumes = APPLICATION_JSON_VALUE,
            tags = {SWAGGER_TAG_POP})
    @PostMapping(path = "/answer")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "access-token", value = "pop access token", required = true, dataType = "string", paramType = "header")
    })
    public Message answerQuestion(@RequestBody @ApiParam PopAnswer popAnswer,
                                  @ApiParam(required = true, value = "The user that use the application")
                                  @RequestParam(value = USER_ID) String userId,
                                  @RequestParam(name = "ANSWER") PopAnswer.AnswerTypeEnum answerTypeEnum) {
        Optional<PopQuestion> question = popQuestionService.findById(popAnswer.getId());
        if (!question.isPresent()) {
            return Message.builder()
                    .message("Question doesn't exist")
                    .build();
        }
        //cannot answer your own question
        PopQuestion popQuestion = question.get();
        if (popQuestion.getUserId().equals(userId)) {
            return Message.builder()
                    .message("You cannot answer your own question")
                    .build();
        }

        //check the timeout - if expired returned
        long now = Instant.now().getEpochSecond();
        long endTime = Instant.ofEpochMilli(popQuestion.getTimeToEnd()).getEpochSecond();
        if (endTime >= now) {
            return Message.builder()
                    .message("Time ended to answer this question")
                    .build();
        }
        //check if i already answered the question
        Answer byUserIdAndByQuestionId = popAnswerService.findByAnswerUserIdAndQuestionId(userId, popAnswer.getId());
        if (byUserIdAndByQuestionId != null) {
            return Message.builder()
                    .message("You already answered the question")
                    .build();
        }

        popAnswerService.save(Answer.builder()
                .askingUserId(popQuestion.getUserId())
                .answerUserId(userId)
                .questionId(popQuestion.getId())
                .answerTypeEnum(answerTypeEnum)
                .description(popQuestion.getDescription())
                .build());
        return Message.builder()
                .message("Success")
                .build();
    }

    @ApiOperation(value = "Get questions",
            consumes = APPLICATION_JSON_VALUE,
            tags = {SWAGGER_TAG_POP})
    @GetMapping("/questions")
    public Questions getQuestions(
            @RequestHeader("access-token") String accessToken) {
        Questions questions = new Questions();
        questions.setQuestions(new ArrayList<>());
        String userId = userTokenService.findByTokenOrderByLocalDateTimeDesc(accessToken, PageRequest.of(0, 1)).get(0).getUserId();
        UserDetails userDetails = popUserService.findById(userId).get();
        List<Topic> userTopics = userDetails.getTopics();

        if (CollectionUtils.isEmpty(userTopics)){
            userTopics = Topic.getAll();
        }

        Set<Answer> myAnswers = popAnswerService.findByAnswerUserId(userId);
        List<PopQuestion> allQuestions = popQuestionService.findQuestionsByUserIdNotAndTopicsIn(userId,userTopics,PageRequest.of(0, 10));
        Collection<String> a = myAnswers.stream().map(Answer::getQuestionId).collect(Collectors.toList());
        questions.setQuestions(allQuestions.stream().filter(question -> !a.contains(question.getId())).collect(Collectors.toList()));
        return questions;
    }

    @ApiOperation(value = "Get statistics for all questions done by the user",
            consumes = APPLICATION_JSON_VALUE,
            tags = {SWAGGER_TAG_POP})
    @GetMapping(path = "/stats")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "access-token", value = "pop access token", required = true, dataType = "string", paramType = "header")
    })
    public List<Statistics> getStatistics(
            @ApiParam(hidden = true) String contentType,
            @ApiParam(required = true, value = "The user that use the application")
            @RequestParam(value = USER_ID) String userId) {

        //find all question by userId
        List<PopQuestion> questionsByUserId = popQuestionService.findQuestionsByUserId(userId);

        //find all questions asked by userId
        Set<Answer> allAnswersByUserId = popAnswerService.findByAskingUserId(userId);

        Set<Statistics> statistics = new HashSet<>();

        //Map of <QuestionID , Answer>
        Map<String, List<Answer>> answersByQuestionId = allAnswersByUserId
                .stream()
                .collect(Collectors.groupingBy(questionIdUserId -> questionIdUserId.getQuestionId()));

        for (PopQuestion popQuestion : questionsByUserId) {
            //all answered questions
            List<Answer> popQuestionIdUserIds = answersByQuestionId.get(popQuestion.getId());
            if (popQuestionIdUserIds == null) {
                popQuestionIdUserIds = new ArrayList<>();
            }

            //count answers by questionId
            Map<PopAnswer.AnswerTypeEnum, Long> answers = popQuestionIdUserIds.stream()
                    .collect(Collectors.groupingBy(Answer::getAnswerTypeEnum, Collectors.counting()));

            Statistics result = Statistics.builder()
                    .option1(answers.get(OPTION1) == null ? 0L : answers.get(OPTION1))
                    .option2(answers.get(OPTION2) == null ? 0L : answers.get(OPTION2))
                    .dontKnow(answers.get(DONT_KNOW) == null ? 0L : answers.get(DONT_KNOW))
                    .description(popQuestion.getDescription())
                    .questionId(popQuestion.getId())
                    .questionCreationTime(popQuestion.getCreationTime())
                    .build();

            statistics.add(result);

        }
        List<Statistics> sort = statistics.stream().sorted(Comparator.comparing(Statistics::getQuestionCreationTime).reversed()).collect(Collectors.toList());
        return sort;
    }

    @ApiOperation(value = "Delete all",
            consumes = APPLICATION_JSON_VALUE,
            tags = {SWAGGER_TAG_POP})
    @DeleteMapping(value = "deleteAll")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "access-token", value = "pop access token", required = true, dataType = "string", paramType = "header")
    })
    public void deleteAll() {
        elasticsearchRepository.forEach(CrudRepository::deleteAll);
    }

}
