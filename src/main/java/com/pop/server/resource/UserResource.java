package com.pop.server.resource;

import com.pop.server.configuration.ApplicationProperties;
import com.pop.server.configuration.SecurityConfig;
import com.pop.server.enums.Gender;
import com.pop.server.enums.Topic;
import com.pop.server.interfaces.PopUserService;
import com.pop.server.pojo.Login;
import com.pop.server.pojo.SignIn;
import com.pop.server.pojo.UserDetails;
import com.pop.server.pojo.UserToken;
import com.pop.server.pojo.facebook.FacebookUser;
import com.pop.server.service.UserTokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.pop.server.constants.ApiConstants.SWAGGER_TAG_USER;
import static com.pop.server.enums.AccountType.FACEBOOK;
import static com.pop.server.enums.Gender.OTHER;
import static com.pop.server.pojo.PopUserTokenStatus.ACTIVE;
import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@RestController
@RequestMapping(value = "/user", produces = APPLICATION_JSON_VALUE)
@Api
@Slf4j
public class UserResource {

    private static final String FACEBOOK_API_VERSION = "v2.11";
    private static final String GRAPH_FACEBOOK_COM = "graph.facebook.com";

    /**
     * Facebook graph url, https
     */
    private static final String FACEBOOK_GRAPH_HTTPS = "https://" + GRAPH_FACEBOOK_COM + "/" + FACEBOOK_API_VERSION + "/";

    /**
     * Facebook authorization url
     */
    private static final String FACEBOOK_DEBUG_TOKEN_URL = FACEBOOK_GRAPH_HTTPS + "/debug_token";
    private static final String FACEBOOKsss = FACEBOOK_GRAPH_HTTPS;
    private static final String INPUT_TOKEN = "input_token";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String ACCOUNT_TYPE = "account_type";

    /**
     * Facebook key of is valid field
     */
    public static final String IS_VALID = "is_valid";

    /**
     * Root json key of facebook response
     */
    public static final String DATA = "data";
    public static final String FACEBOOK_USER_ID = "user_id";
    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_SECRET = "client_secret";
    public static final String GRANT_TYPE = "grant_type";
    public static final String CLIENT_CREDENTIALS = "client_credentials";

    @Autowired
    private PopUserService popUserService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private UserTokenService userTokenService;

    private String facebookAccessTokenUrl;

    @PostConstruct
    public void init() {
        facebookAccessTokenUrl = UriComponentsBuilder.fromUriString(FACEBOOKsss)
                .path("oauth/access_token")
                .queryParam(CLIENT_ID, applicationProperties.getFacebookAppId())
                .queryParam(CLIENT_SECRET, applicationProperties.getFacebookAppKey())
                .queryParam(GRANT_TYPE, CLIENT_CREDENTIALS).toUriString();
    }

    @ApiOperation(value = "Sign in",
            notes = "Sign in to pop with external access token",
            consumes = MULTIPART_FORM_DATA_VALUE,
            tags = {SWAGGER_TAG_USER})
    @PostMapping(value = "/signin")
    public UserToken signIn(@RequestBody @ApiParam SignIn signIn) {
        UserDetails userDetails = null;
        switch (signIn.getAccountType()) {
            case GOOGLE:
                userDetails = handleGoogleUser(signIn.getAccessToken());
                break;
            case FACEBOOK:
                userDetails = handleFacebookUser(signIn.getAccessToken());
                break;
        }

        // Search for user token. new user can have a valid token when reinstall the client
        return getUserToken(userDetails);
    }

    private UserToken getUserToken(UserDetails userDetails) {
        List<UserToken> userTokens = userTokenService.findByUserIdOrderByLocalDateTimeDesc(userDetails.getId(), PageRequest.of(0, 1));
        if (!CollectionUtils.isEmpty(userTokens)){
            long userTokenTime = userTokens.get(0).getLocalDateTime().getTime();
            if (HOURS.convert(System.currentTimeMillis() - userTokenTime, MILLISECONDS) <= SecurityConfig.SESSION_EXPIRE_TIME_HOURS) {
                return userTokens.get(0);
            }
        }

        UserToken userToken = UserToken.builder()
                .token(UUID.randomUUID().toString())
                .localDateTime(Calendar.getInstance().getTime())
                .popUserTokenStatus(ACTIVE)
                .userId(userDetails.getId())
                .build();
        userToken = userTokenService.save(userToken);
        return userToken;
    }

    @ApiOperation(value = "Login",
            notes = "Login into pop",
            consumes = MULTIPART_FORM_DATA_VALUE,
            tags = {SWAGGER_TAG_USER})
    @PostMapping(value = "/login")
    public UserToken login(@RequestBody @ApiParam Login login) {
        String externalUserId = null;
        switch (login.getAccountType()) {
            case GOOGLE:
                externalUserId = validateGoogleToken(login.getAccessToken());
                break;
            case FACEBOOK:
                String accessToken = getFacebookAccessToken();
                externalUserId = validateFacebookToken(login.getAccessToken(), accessToken);
                break;
        }
        UserDetails userDetails = popUserService.findByAccountIdAndAccountType(externalUserId, login.getAccountType());

        // In login api user must be registered to pop it
        if (userDetails == null) {
            if (log.isDebugEnabled()) {
                log.debug("Login failed for request: {}, external id is:{}", login, externalUserId);
            }
            throw new RuntimeException("Login failed");
        }

        return getUserToken(userDetails);
    }

    @ApiOperation(value = "Update user topics",
            notes = "Update the topics of the user",
            consumes = MULTIPART_FORM_DATA_VALUE,
            tags = {SWAGGER_TAG_USER})
    @PostMapping()
    public UserDetails editUser(@RequestHeader("access-token") String accessToken,
                                @RequestBody @Valid @NotEmpty @ApiParam(name = "topics", value = "Collection of TOPIC enum", example = "0") List<@NotNull Topic> topics){
        String userId = userTokenService.findByTokenOrderByLocalDateTimeDesc(accessToken, PageRequest.of(0, 1)).get(0).getUserId();
        UserDetails userDetails = popUserService.findById(userId).get();
        userDetails.setTopics(topics);
        userDetails = popUserService.save(userDetails);
        return userDetails;
    }

    @ApiOperation(value = "Get a user",
            notes = "Get the details of the user",
            consumes = MULTIPART_FORM_DATA_VALUE,
            tags = {SWAGGER_TAG_USER})
    @GetMapping()
    public @ResponseBody UserDetails getUser(@RequestHeader("access-token") String accessToken){
            String userId = userTokenService.findByTokenOrderByLocalDateTimeDesc(accessToken, PageRequest.of(0, 1)).get(0).getUserId();
        return popUserService.findById(userId).get();
    }

    private String getFacebookAccessToken() {
        Map accessTokenResponse = restTemplate.getForEntity(facebookAccessTokenUrl, Map.class).getBody();
        return String.valueOf(accessTokenResponse.get(ACCESS_TOKEN));
    }

    private String validateGoogleToken(String token) {
        return null;
    }

    private UserDetails handleGoogleUser(String token) {
        return null;
    }

    private UserDetails handleFacebookUser(String token) {
        String accessToken = getFacebookAccessToken();
        String facebookUserId = validateFacebookToken(token, accessToken);

        // Token is valid, search for pop user
        UserDetails popUserDetails = popUserService.findByAccountIdAndAccountType(facebookUserId, FACEBOOK);
        if (popUserDetails == null) {
            // Pop user was not found retrieve user data from facebook and add it as pop user
            FacebookUser facebookUser = restTemplate.getForEntity(
                    UriComponentsBuilder.fromUriString(FACEBOOK_GRAPH_HTTPS)
                            .path(facebookUserId)
                            .queryParam(ACCESS_TOKEN, accessToken)
                            .build()
                            .toUri(), FacebookUser.class).getBody();
            // Convert facebook user to user details and save
            popUserDetails = convertToUser(facebookUserId, facebookUser);
            popUserDetails = popUserService.save(popUserDetails);
        }
        return popUserDetails;
    }

    private String validateFacebookToken(String userToken, String accessToken) {

        // Validate token against facebook api
        Map response = restTemplate.getForEntity(UriComponentsBuilder.fromUriString(FACEBOOK_DEBUG_TOKEN_URL)
                .queryParam(ACCESS_TOKEN, accessToken)
                .queryParam(INPUT_TOKEN, userToken)
                .build()
                .toUri(), Map.class).getBody();

        if (CollectionUtils.isEmpty(response)) {
            if (log.isWarnEnabled()) {
                log.warn("Invalid response from facebook user token is: {}", userToken);
            }
            throw new RuntimeException("");
        }

        Map dataResponse = (Map) response.get(DATA);
        if (StringUtils.isEmpty(dataResponse.get(IS_VALID))) {
            if (log.isWarnEnabled()) {
                log.warn("Facebook response contains invalid status for: is_valid field for user token: {}", userToken);
            }
            throw new RuntimeException("");
        }

        if (!Boolean.valueOf(String.valueOf(dataResponse.get(IS_VALID)))) {
            throw new RuntimeException("");
        }

        String facebookUserId = String.valueOf(dataResponse.get(FACEBOOK_USER_ID));
        if (log.isDebugEnabled()) {
            log.debug("Query user: {}", facebookUserId);
        }
        return facebookUserId;
    }

    private UserDetails convertToUser(String facebookUserId, FacebookUser facebookUser) {
        Gender gender;
        if (StringUtils.isEmpty(facebookUser.getGender())){
            gender = OTHER;
        }else {
            try {

                gender = Gender.valueOf(facebookUser.getGender());
            } catch (IllegalArgumentException e) {
                gender = OTHER;
            }
        }
        return UserDetails.builder()
                .accountType(FACEBOOK)
                .accountId(facebookUserId)
                .birthday(facebookUser.getBirthday())
                .email(facebookUser.getEmail())
                .firstName(facebookUser.getFirst_name())
                .fullName(facebookUser.getName())
                .gender(gender)
                .lastName(facebookUser.getLast_name())
                .middleName(facebookUser.getMiddle_name())
                .build();
    }
}
