package com.pop.server.configuration;

import com.pop.server.pojo.UserToken;
import com.pop.server.service.UserTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.util.CollectionUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

@EnableWebSecurity
public class SecurityConfig {
    public static long SESSION_EXPIRE_TIME_HOURS = 1;

    @Configuration
    @Order(1)
    public static class HeaderWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

        @Autowired
        UserTokenService userTokenService;

        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable()
                    .addFilter(preAuthFilter())
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .cors()
                    .and()
                    .authorizeRequests()
                    .mvcMatchers("/api/**", "/user/**")
                    .authenticated()
//                    .and()
//                    .authorizeRequests().mvcMatchers("/user/login/**", "/user/signin").permitAll()
            .filterSecurityInterceptorOncePerRequest(true);
        }

        RequestHeaderAuthenticationFilter preAuthFilter() {
            RequestHeaderAuthenticationFilter filter = new RequestHeaderAuthenticationFilter();
            filter.setAuthenticationManager(popAuthenticationManager());
            filter.setAuthenticationFailureHandler(popAuthenticationFailureHandler());
            filter.setPrincipalRequestHeader("ACCESS-TOKEN");
            filter.setExceptionIfHeaderMissing(false);
            filter.setContinueFilterChainOnUnsuccessfulAuthentication(false);
            return filter;
        }

        AuthenticationFailureHandler popAuthenticationFailureHandler() {
            return (request, response, exception) -> {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                ServletOutputStream out = response.getOutputStream();
                out.println("Authentication error");
                out.close();
            };
        }

        AuthenticationManager popAuthenticationManager() {
            return authentication -> {
                String accessToken = authentication.getName();
                List<UserToken> userTokens = userTokenService.findByTokenOrderByLocalDateTimeDesc(accessToken, PageRequest.of(0, 1));
                if (CollectionUtils.isEmpty(userTokens)) {
                    throw new BadCredentialsException("Token not found [" + accessToken + "]");
                }
                long userTokenTime = userTokens.get(0).getLocalDateTime().getTime();

                if (HOURS.convert(System.currentTimeMillis() - userTokenTime, MILLISECONDS) > SESSION_EXPIRE_TIME_HOURS) {
                    throw new BadCredentialsException("Token not found [" + accessToken + "]");
                }
                authentication.setAuthenticated(true);
                return authentication;
            };
        }
    }

    @Configuration
    public static class BasicWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
        @Autowired
        public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
            auth
                    .inMemoryAuthentication()
                    .withUser("popit123").password("{noop}popit123").roles("USER");
        }
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable()
            .mvcMatcher("/**")
                    .httpBasic()
                    .and()
                    .authorizeRequests()
                    .mvcMatchers("/**").hasRole("USER");
        }

        @Override
        public void configure(WebSecurity web) {
           web.ignoring().antMatchers("/images/**", "/user/login", "/user/signin");
        }
    }
}
