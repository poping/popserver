package com.pop.server.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class ApplicationProperties {

    private String imagesFolder;

    @Value("${pop.facebook.secret}")
    private String facebookAppKey;

    @Value("${pop.facebook.app.id}")
    private String facebookAppId;

    @Value("${pop.images-folder.path}")
    public void setImagesFolder(String imagesFolder){
        if (!imagesFolder.endsWith(File.separator)){
            imagesFolder += File.separator;
        }
        this.imagesFolder = imagesFolder;
    }

    public String getImagesFolder(){
        return imagesFolder;
    }

    public String getFacebookAppKey() {
        return facebookAppKey;
    }

    public String getFacebookAppId() {
        return facebookAppId;
    }
}
