package com.pop.server.constants;

public interface ApiConstants {

    String MULTIPART_FILE = "file";

    String SWAGGER_TAG_POP = "POP";
    String SWAGGER_TAG_USER = "User";
    String SWAGGER_TAG_FACEBOOK = "Facebook";
    String USER_ID = "userId";

    String FROM_PAGE = "fromPage";

    String USER_NAME = "userName";

    String REGISTER = "/register";

    String LOGIN = "/login";
    int IMAGE_MAX_WIDTH = 1024;

    int IMAGE_MAX_HEIGHT = 1024;
}
