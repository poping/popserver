package com.pop.server.interfaces;

import com.pop.server.enums.AccountType;
import com.pop.server.pojo.UserDetails;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface PopUserService extends ElasticsearchRepository<UserDetails, String> {
    UserDetails findByAccountIdAndAccountType(String userId, AccountType accountType);
}