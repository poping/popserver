package com.pop.server.interfaces;

import com.pop.server.pojo.Answer;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Set;

public interface PopAnswerService extends ElasticsearchRepository<Answer, String> {

    Set<Answer> findByAskingUserId(String askingUserId);
    Set<Answer> findByAnswerUserId(String answerUserId);
    Answer findByAnswerUserIdAndQuestionId(String answerUserId, String questionId);
}