package com.pop.server.interfaces;

import com.pop.server.enums.Topic;
import com.pop.server.pojo.PopQuestion;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Collection;
import java.util.List;

public interface PopQuestionService extends ElasticsearchRepository<PopQuestion, String> {

    List<PopQuestion> findQuestionsByUserId(String userId);

    // get all questions where userId is not the given one, and the topics of the questions are as same as topics parameter
    List<PopQuestion> findQuestionsByUserIdNotAndTopicsIn(String userId, Collection<Topic> topics, Pageable pageRequest);
}