import org.elasticsearch.common.network.NetworkModule;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.InternalSettingsPreparer;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeValidationException;
import org.elasticsearch.plugins.Plugin;
import org.elasticsearch.transport.Netty4Plugin;

import java.util.Collection;
import java.util.Collections;

public class StartEs {
    public static void main(String[] args) throws NodeValidationException {
        Settings elasticsearchSettings = Settings.builder()
                .put("http.enabled", "true")
                .put("cluster.name", "docker-cluster")
                .put("path.data", "es-data")
                .put("path.home", "es-home")
                .put("discovery.type", "single-node")
                .put("script.inline", "true")
                .put("network.host", "localhost")
                .put(NetworkModule.HTTP_TYPE_KEY, "netty4")
                .put("http.port", 9200)
                .put("transport.tcp.port", 9300)
                .build();

        Node node = new ElasticPluginConfigurableNode(elasticsearchSettings, Collections.singletonList(Netty4Plugin.class));
        node.start();
        while(true);

    }
    private static class ElasticPluginConfigurableNode extends Node {
        public ElasticPluginConfigurableNode(Settings settings, Collection<Class<? extends Plugin>> classpathPlugins) {
            super(InternalSettingsPreparer.prepareEnvironment(settings, null), classpathPlugins);
        }
    }
}
